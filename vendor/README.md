
# Vendored sources for handlebars.c

## sort_r

* Repo: [https://github.com/noporpoise/sort_r](https://github.com/noporpoise/sort_r)
* Ref: [c8c65c1e183df8f3bd1b221e27653d9705ff4abe](https://github.com/noporpoise/sort_r/commit/c8c65c1e183df8f3bd1b221e27653d9705ff4abe)

## xxhash

* Repo: [https://github.com/Cyan4973/xxHash](https://github.com/Cyan4973/xxHash)
* Ref: [v0.7.3](https://github.com/Cyan4973/xxHash/tree/v0.7.3)
